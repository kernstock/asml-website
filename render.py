from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader('templates'))

sites = env.list_templates()
sites.remove('base.html')
for site in sites:
    index = env.get_template(site)
    with open(site, "w") as f:
        f.write(index.render())
